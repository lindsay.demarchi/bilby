.. _run_sampler:

========
Sampling
========

Given a :ref:`likelihood` and :ref:`priors`, we can run parameter estimation using the
`run_sampler` function. This can be accessed via :code:`bilby.run_sampler` or
:code:`bilby.sampler.run_sampler`. Here is the detailed API:

.. autofunction:: bilby.run_sampler
